_**CNN**_

In deep learning, a convolutional neural network (CNN, or ConvNet) is a class of deep neural networks, most commonly applied to analyzing visual imagery. They have applications in image and video recognition, recommender systems, image classification, medical image analysis etc.

**Important Terms for CNNs:**
1. Kernel Size(k)
2. Stride(s)
3. Padding(p)
4. Filters(f)
5. Flatten


Working of a CNN:
![](https://gitlab.com/iotiotdotin/ai-user-training/-/wikis/uploads/8280d7f007a9f7d07eae922ea7102477/dnn2.png)